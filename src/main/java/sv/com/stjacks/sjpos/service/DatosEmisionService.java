package sv.com.stjacks.sjpos.service;

import java.util.Arrays;
import java.util.UUID;
import javax.ws.rs.ClientErrorException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;


@Service
public class DatosEmisionService {
	@Value("${path.server}")
	private String server;

	private RestTemplate clienteRest = new RestTemplate();
	
// inicio de la clase interna 
	private WebTarget webTarget;
	private Client client;
//	private static final String BASE_URI = "https://dev.api.ifacere-fel.com/fel-dte-services/";
	private static final String BASE_URI2 = "https://dev2.api.ifacere-fel.com/";
    private static final String BASE_URI3 = "https://dev.api.soluciones-mega.com/";
	private static final String BASE_URI4 = "http://service.pos.stjacks.com";
	
	String tocken = "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJzY29wZSI6WyJvcGVuaWQiXSwiZXhwIjoxNTkzNzIzMDEwLCJhdXRob3JpdGllcyI6WyJST0xFX0VNSVNPUiJdLCJqdGkiOiI0M2E1MjM1ZS01M2ViLTQyOGQtOGRmYS1hZjc5ZGFmNGMwNDciLCJjbGllbnRfaWQiOiIyNTI1NDY0MiJ9.0BnJ8ach6EpTQU8voUmcz8h-9eBInY3Hoqjze79YD8mLn1wlbnJ079fRhyDgwc04ZWTAePAkm1maZw_elqmCXu5hEvDIeTmdGBI7Y4tzbVm2HrFdsOu-wpN-7nRmfyh1q17H6gGJ7SzhD9Vr9VVTiO2CilVIHx9hPlASFDWD1DyBMAWbIemnpItz0UeEqSh_-3To4fpZzEu8AXCVIBAxMJ5_19AmKERlPOZYUj2UOuv85fKrlpALZdNx5yf8byLVMsNh11zYw3gCm5uyaInnUlD-msqeJKYB89R0zVW84WOQxaeQFZyhRXzhY6BrRzkzbEZ6TVH5hKOhgGPxmQuPDw";

	String usuario = "25254642";
	
	String key ="0i2jwN3eF45Tw9OrDnh3atr";
	
	String xmlTocken;
	String xml;
	
	public String getDocument(Long idVenta) {
		String doc="nada";
		System.out.println(clienteRest);
		try {
		 doc = clienteRest.getForObject( BASE_URI4 + ":8091/api/v1/felguate-service/facturaElectronica/" + idVenta, String.class);
		}catch(Exception e) {
			System.out.println("Error " + e.getMessage());
			e.printStackTrace();
		}
		System.out.println(doc);
		return doc;
	}
	
	public void clientTarget() {
		client = ClientBuilder.newClient();
		webTarget = client.target(BASE_URI2).path("api");
	}

	public DatosEmisionService() {
		clientTarget();
		tocken = "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJzY29wZSI6WyJvcGVuaWQiXSwiZXhwIjoxNTkzNzIzMDEwLCJhdXRob3JpdGllcyI6WyJST0xFX0VNSVNPUiJdLCJqdGkiOiI0M2E1MjM1ZS01M2ViLTQyOGQtOGRmYS1hZjc5ZGFmNGMwNDciLCJjbGllbnRfaWQiOiIyNTI1NDY0MiJ9.0BnJ8ach6EpTQU8voUmcz8h-9eBInY3Hoqjze79YD8mLn1wlbnJ079fRhyDgwc04ZWTAePAkm1maZw_elqmCXu5hEvDIeTmdGBI7Y4tzbVm2HrFdsOu-wpN-7nRmfyh1q17H6gGJ7SzhD9Vr9VVTiO2CilVIHx9hPlASFDWD1DyBMAWbIemnpItz0UeEqSh_-3To4fpZzEu8AXCVIBAxMJ5_19AmKERlPOZYUj2UOuv85fKrlpALZdNx5yf8byLVMsNh11zYw3gCm5uyaInnUlD-msqeJKYB89R0zVW84WOQxaeQFZyhRXzhY6BrRzkzbEZ6TVH5hKOhgGPxmQuPDw";
	}
	
	public void solicitaTocken() {
		clientTarget();
		//  XML en formato de String
		xmlTocken = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
		+ "<SolicitaTokenRequest>\n"
		+ "<usuario>" + usuario + "</usuario>\n" 
		+ "<apikey>" + key + "</apikey>\n"
		+ "</SolicitaTokenRequest>";
		
		//se envia el xml y se recibe el tocken en una variable String 
		tocken = getTocken(xmlTocken);
		webTarget = client.target(BASE_URI2).path("api");
	}

	public String getTocken(String requestEntity) throws ClientErrorException {
		// inicio método
		clientTarget();
		tocken = webTarget.path("solicitarToken").request(MediaType.APPLICATION_XML)
				.post(Entity.entity(requestEntity, MediaType.APPLICATION_XML)).readEntity(String.class);
		int start = tocken.indexOf("<token>") + "<token>".length();
		int end = tocken.lastIndexOf("</token>");
		tocken = tocken.substring(start, end);
		return tocken;
	}
	// fin de método
	
	public String introduceTocken(String tocken) throws ClientErrorException {
		// inicio método
		this.tocken = tocken;
		return this.tocken;
	}
        
    public String getFirm(String requestEntity, String t){
		// método por utilizar y envío del token
        client = ClientBuilder.newClient();
		webTarget = client.target(BASE_URI3).path("api");
		try {
		xml = webTarget.path("solicitaFirma").request(MediaType.APPLICATION_XML)
				.header(HttpHeaders.AUTHORIZATION, "Bearer " + t).header("Content-type", "application/xml;charset=UTF-8")
				.post(Entity.entity(requestEntity, MediaType.APPLICATION_XML)).readEntity(String.class);
		}catch(Exception e) {
			System.out.println("Error " + e.getMessage());
			e.printStackTrace();
		}
		return xml;
	}
        
	public String getAuth(String requestEntity, String t) throws ClientErrorException {
		// método por utilizar y envío del token
		clientTarget();
		return webTarget.path("registrarDocumentoXML").request(MediaType.APPLICATION_XML)
				.header(HttpHeaders.AUTHORIZATION, "Bearer " + t).header("Content-type", "application/xml;charset=UTF-8")
				.post(Entity.entity(requestEntity, MediaType.APPLICATION_XML)).readEntity(String.class);
	}

	public String getXml(String requestEntity) throws ClientErrorException {
		clientTarget();
		return webTarget.path("retornarXML").request(MediaType.APPLICATION_XML)
				.header(HttpHeaders.AUTHORIZATION, "Bearer " + tocken).header("Content-type", "application/xml;charset=UTF-8")
				.post(Entity.entity(requestEntity, MediaType.APPLICATION_XML)).readEntity(String.class);
	}

	public String getPdf(String requestEntity) throws ClientErrorException {
		clientTarget();
		return webTarget.path("retornarPDF").request(MediaType.APPLICATION_XML)
				.header(HttpHeaders.AUTHORIZATION, "Bearer " + tocken).header("Content-type", "application/xml;charset=UTF-8")
				.post(Entity.entity(requestEntity, MediaType.APPLICATION_XML)).readEntity(String.class);
	}

	public String anuleXml(String requestEntity) throws ClientErrorException {
		clientTarget();
		return webTarget.path("anularDocumentoXML").request(MediaType.APPLICATION_XML)
				.header(HttpHeaders.AUTHORIZATION, "Bearer " + tocken).header("Content-type", "application/xml;charset=UTF-8")
				.post(Entity.entity(requestEntity, MediaType.APPLICATION_XML)).readEntity(String.class);
	}

	public String getClient(String requestEntity) throws ClientErrorException {
		clientTarget();
		return webTarget.path("RetornaDatosCliente").request(MediaType.APPLICATION_XML)
				.header(HttpHeaders.AUTHORIZATION, "Bearer " + tocken).header("Content-type", "application/xml;charset=UTF-8")
				.post(Entity.entity(requestEntity, MediaType.APPLICATION_XML)).readEntity(String.class);
	}
	
	public String getXmlPos(UUID uuid) {
		
		return "";
	}

	public boolean close() {
		// inicio de método
		try{
			client.close();
			return true;
		}catch(Exception e) {
			System.out.println("Error " + e.getMessage());
			e.printStackTrace();
			return false;
		}
	}
	// fin de método
}