package sv.com.stjacks.sjpos.controller;

import java.security.Principal;
import java.util.Collections;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import sv.com.stjacks.sjpos.entities.DocumentoVenta;
import sv.com.stjacks.sjpos.service.DatosEmisionService;
import sv.com.stjacks.sjpos.service.DocumentoVentaService;


@RestController
@RequestMapping("xml")
public class XmlController {
	
	@Autowired
	DatosEmisionService datosEmisionService;

	final Logger logger = LoggerFactory.getLogger(XmlController.class);

	/*
	 * Obtiene la firma de la factura
	 */
	@PostMapping("firm")
	public String getFirm(@RequestBody String xml) {
		String tocken = "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJzY29wZSI6WyJvcGVuaWQiXSwiZXhwIjoxNTkzNzIzMDEwLCJhdXRob3JpdGllcyI6WyJST0xFX0VNSVNPUiJdLCJqdGkiOiI0M2E1MjM1ZS01M2ViLTQyOGQtOGRmYS1hZjc5ZGFmNGMwNDciLCJjbGllbnRfaWQiOiIyNTI1NDY0MiJ9.0BnJ8ach6EpTQU8voUmcz8h-9eBInY3Hoqjze79YD8mLn1wlbnJ079fRhyDgwc04ZWTAePAkm1maZw_elqmCXu5hEvDIeTmdGBI7Y4tzbVm2HrFdsOu-wpN-7nRmfyh1q17H6gGJ7SzhD9Vr9VVTiO2CilVIHx9hPlASFDWD1DyBMAWbIemnpItz0UeEqSh_-3To4fpZzEu8AXCVIBAxMJ5_19AmKERlPOZYUj2UOuv85fKrlpALZdNx5yf8byLVMsNh11zYw3gCm5uyaInnUlD-msqeJKYB89R0zVW84WOQxaeQFZyhRXzhY6BrRzkzbEZ6TVH5hKOhgGPxmQuPDw";
		//se envia el xml con los datos del documento
		String auth = datosEmisionService.getFirm(xml, tocken);
		String xmlFirm = auth.replaceAll("&lt;", "<");
		xmlFirm = xmlFirm.replaceAll("&gt;", ">");
		xmlFirm = xmlFirm.replaceAll("&amp;", "&");
		xmlFirm = xmlFirm.substring(88,xmlFirm.length()-1);
		xmlFirm = xmlFirm.substring(0,xmlFirm.length()-135);
		System.out.println(xmlFirm);
		return xmlFirm;
	}
	
	/*
	 * Obtiene la certificacion de la factura
	 */
	@PostMapping("certif")
	public String getAutorizacion(@RequestBody String xmlFirmado) {
		System.out.println(xmlFirmado);
		String tocken = "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJzY29wZSI6WyJvcGVuaWQiXSwiZXhwIjoxNTkzNzIzMDEwLCJhdXRob3JpdGllcyI6WyJST0xFX0VNSVNPUiJdLCJqdGkiOiI0M2E1MjM1ZS01M2ViLTQyOGQtOGRmYS1hZjc5ZGFmNGMwNDciLCJjbGllbnRfaWQiOiIyNTI1NDY0MiJ9.0BnJ8ach6EpTQU8voUmcz8h-9eBInY3Hoqjze79YD8mLn1wlbnJ079fRhyDgwc04ZWTAePAkm1maZw_elqmCXu5hEvDIeTmdGBI7Y4tzbVm2HrFdsOu-wpN-7nRmfyh1q17H6gGJ7SzhD9Vr9VVTiO2CilVIHx9hPlASFDWD1DyBMAWbIemnpItz0UeEqSh_-3To4fpZzEu8AXCVIBAxMJ5_19AmKERlPOZYUj2UOuv85fKrlpALZdNx5yf8byLVMsNh11zYw3gCm5uyaInnUlD-msqeJKYB89R0zVW84WOQxaeQFZyhRXzhY6BrRzkzbEZ6TVH5hKOhgGPxmQuPDw";
		//se envia el xml con los datos del documento
		String auth = datosEmisionService.getAuth(xmlFirmado, tocken);
		String xmlFirm = auth.replaceAll("&lt;", "<");
		xmlFirm = xmlFirm.replaceAll("&gt;", ">");
		xmlFirm = xmlFirm.replaceAll("&amp;", "&");
		System.out.println(xmlFirm);
		return xmlFirm;
	}	
}